# Gulp

## Links

- Curso de Gulp Free Udemy => https://www.udemy.com/curso-de-gulp/ 


## Tarefas do Gulp

- Tasks()   => Tarefas no gulp;
- src()     => Arquivos que entraram no fluxo de tarefas para serem tratados ou manipulados;
- dest()    => Destino dos arquivos que já passaram pelo fluxo de tarefa;
- watch()   => Assiste/Observa os arquivos e faz alguma tarefa quando esses são alterados;
- pipe()    => concatena tarefas no gulp.

